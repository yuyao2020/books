## 豆瓣 2019 书单对应文件

书的内容简价可以在 [豆瓣 2019 年度榜单](https://book.douban.com/annual/2019?source=navigation) 中查阅。 

- 格式 `mobi`, `azw3`, `epub`, `pdf`
- 总大小 525M 

<p align="center"> <img width="666" alt="books-of-year" src="img/dir.png" style="border-radius: 20px"/><p>

